<?php
/**
 * POSTデータのクラス
 */

namespace Sharecoto\Request;

class Post extends RequestAbstract
{
    public function __get($name)
    {
        if (array_key_exists($name, $this)) {
            return $this[$name];
        }
        return null;
    }
}

