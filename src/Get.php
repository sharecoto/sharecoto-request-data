<?php
namespace Sharecoto\Request;

class Get extends RequestAbstract
{
    public function __get($name)
    {
        if (array_key_exists($name, $this)) {
            return $this[$name];
        }
        return null;
    }
}

