<?php

/**
 * $_REQUESTとかをラップするクラス
 */

namespace Sharecoto\Request;

abstract class RequestAbstract extends \ArrayObject
{
    protected $jsonOption;

    public function __construct($input = [] ,$flags = 0, $iterator_class = "ArrayIterator")
    {
        parent::__construct($input ,$flags, $iterator_class);

        $this->setJsonOption();
    }

    public function toJson($option = null)
    {
        if (!is_int($option)) {
            $option = $this->jsonOption;
        }
        return json_encode($this, $option);
    }

    public function setJsonOption($option = null)
    {
        if (!is_int($option)) {
            $option = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_PRETTY_PRINT;
        }
        $this->jsonOption = $option;
        return $this;
    }
}
